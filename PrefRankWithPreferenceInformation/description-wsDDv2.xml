<?xml version='1.0' encoding='utf-8'?>
<program_description>
	<program provider="PUT" name="Outranking-PrefRank-WithPreferenceInformation_scores" version="1.0.0" displayName="Outranking-PrefRank-WithPreferenceInformation_scores"/>
	<documentation>
		<description>Module for calculation PrefRank scores using preference information given by decision maker.</description>
		<contact><![CDATA[Krzysztof Martyn <krzysztof.martyn@wp.pl>]]></contact>
		<url>https://bitbucket.org/Krzysztof_Martyn/prefrank</url>
	</documentation>
	<parameters>
		<input id="input1" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>Alternatives to consider.</description>
			</documentation>
			<xmcda tag="alternatives"/>
		</input>
		<input id="input2" name="preferences" displayName="preferences" isoptional="0">
			<documentation>
				<description>Aggregated preferences matrix.</description>
			</documentation>
			<xmcda tag="alternativesComparisons"/>
		</input>
		<input id="input3" name="preference_information" displayName="preference information" isoptional="0">
			<documentation>
				<description>Preference information about alternatives. Strength for the alternative is given to the value of true, weakness for false, when no value is given, the alternative does not gain strength or weakness.</description>
			</documentation>
			<xmcda tag="alternativesValues"/>
		</input>
		<input id="input4" name="parameters" displayName="parameters" isoptional="0">
			<documentation>
				<description>Parameter specifies the algorithm to calculate ranking. There are three algorithms to choose from: PageRank, HITS and Salsa.
				</description>
			</documentation>
			<xmcda tag="methodParameters"><![CDATA[
        
		<methodParameters>
			<parameter id="algorithm_type" name="algorithm_type">
				<value>
					<label>%1</label>
				</value>
			</parameter>
			<parameter id="q" name="q">
				<value>
					<real>%2</real>
				</value>
			</parameter>
			<parameter id="number_of_iteration" name="number_of_iteration">
				<value>
					<integer>%3</integer>
				</value>
			</parameter>
			<parameter id="check_convergence" name="check_convergence">
				<value>
					<boolean>%4</boolean>
				</value>
			</parameter>
			<parameter id="early_stopping" name="early_stopping">
				<value>
					<boolean>%5</boolean>
				</value>
			</parameter>
		</methodParameters>
			
      ]]></xmcda>
			<gui status="preferGUI">
				<entry id="%1" type="enum" displayName="algorithm type">
					<items>
						<item id="item0">
							<description>PrefRank with preference information I</description>
							<value>prefrank_with_preference_information_1</value>
						</item>
						<item id="item1">
							<description>PrefRank with preference information II</description>
							<value>prefrank_with_preference_information_2</value>
						</item>
						<item id="item2">
							<description>PrefRank with preference information III</description>
							<value>prefrank_with_preference_information_3</value>
						</item>
					</items>
					<defaultValue>item0</defaultValue>
				</entry>
				<entry id="%2" type="float" displayName="damping factor">
					<documentation>
						<description>Damping factor</description>
					</documentation>
					<constraint>
						<description>The real value must be between 0 and 1</description>
						<code><![CDATA[ %2 >= 0 && %2 <= 1 ]]></code>
					</constraint>
					<defaultValue>0.15</defaultValue>
				</entry>
				<entry id="%3" type="int" displayName="number of iteration">
					<documentation>
						<description>Number of iteration</description>
					</documentation>
					<constraint>
						<description>The value should be greater than 1</description>
						<code><![CDATA[ %3 > 1]]></code>
					</constraint>
					<defaultValue>100</defaultValue>
				</entry>
				<entry id="%4" type="boolean" displayName="Run averaging if not converge?">
					<documentation>
						<description>Turning on the version with averaging the score value, if after "number of iteration" iteration the score does not converge, that is: the maximum score difference between the last two iterations is greater than 0.001.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
				<entry id="%5" type="boolean" displayName="Earlier stop if converge?">
					<documentation>
						<description>Earlier stop if the maximum difference in score between two consecutive iterations is less than 0.00001</description>
					</documentation>
					<defaultValue>true</defaultValue>
				</entry>
			</gui>
		</input>
		<output id="output1" name="positive_flows" displayName="positive flows">
			<documentation>
				<description>Positive outranking flows.</description>
			</documentation>
			<xmcda tag="alternativesValues"/>
		</output>
		<output id="output2" name="negative_flows" displayName="negative flows">
			<documentation>
				<description>Negative outranking flows.</description>
			</documentation>
			<xmcda tag="alternativesValues"/>
		</output>
		<output id="output3" name="total_flows" displayName="total flows">
			<documentation>
				<description>Final flows computed from the given data.</description>
			</documentation>
			<xmcda tag="alternativesValues"/>
		</output>
		<output id="output4" name="ranking" displayName="ranking">
			<documentation>
				<description>PrefRank scores computed from the given data.</description>
			</documentation>
			<xmcda tag="alternativesComparisons"/>
		</output>
		<output id="output5" name="messages" displayName="messages">
			<documentation>
				<description>Messages or errors generated by this module.</description>
			</documentation>
			<xmcda tag="methodMessages"/>
		</output>
	</parameters>
</program_description>
