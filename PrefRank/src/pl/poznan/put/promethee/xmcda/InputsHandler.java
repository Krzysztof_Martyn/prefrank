package pl.poznan.put.promethee.xmcda;

import org.xmcda.Alternative;
import org.xmcda.AlternativesMatrix;
import org.xmcda.ProgramExecutionResult;
import org.xmcda.ProgramParameter;
import org.xmcda.XMCDA;
import org.xmcda.utils.Coord;
import org.xmcda.utils.ValueConverters.ConversionException;

import pl.poznan.put.promethee.xmcda.InputsHandler.Inputs;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 
 */
public class InputsHandler {	
	public enum AlgorithmType {
		TYPE_1("prefrank_1"), TYPE_2("prefrank_2"),TYPE_3("prefrank_3");

		private String label;

		private AlgorithmType(String paramLabel) {
			label = paramLabel;
		}

		/**
		 * Return the label for this AlgorithmType Parameter
		 *
		 * @return the parameter's label
		 */
		public final String getLabel() {
			return label;
		}

		/**
		 * Returns the parameter's label
		 *
		 * @return the parameter's label
		 */
		@Override
		public String toString() {
			return label;
		}

		/**
		 * Returns the {@link AlgorithmType} with the specified label. It
		 * behaves like {@link #valueOf(String)} with the exception
		 *
		 * @param parameterLabel
		 *            the label of the constant to return
		 * @return the enum constant with the specified label
		 * @throws IllegalArgumentException
		 *             if there is no AlgorithmType with this label
		 * @throws NullPointerException
		 *             if parameterLabel is null
		 */
		public static AlgorithmType fromString(String parameterLabel) {
			if (parameterLabel == null)
				throw new NullPointerException("parameterLabel is null");
			for (AlgorithmType op : AlgorithmType.values()) {
				if (op.toString().equals(parameterLabel))
					return op;
			}
			throw new IllegalArgumentException("Enum AlgorithmType with label " + parameterLabel + " not found");
		}
	}
	
	
	
	public static class Inputs {
		public AlgorithmType algorithmType;
		public List<String> alternatives_ids;
		public Map<String, Map<String, Double>> preferences;
		public boolean checkConvergence;
		public int numberOfIteration;
		public boolean earlyStopping;
	}

	/**
	 *
	 * @param xmcda
	 * @param xmcda_exec_results
	 * @return
	 */
	static public Inputs checkAndExtractInputs(XMCDA xmcda, ProgramExecutionResult xmcda_exec_results) {
		Inputs inputs = new Inputs();
		checkParameters(inputs, xmcda, xmcda_exec_results);
		checkAlternatives(inputs, xmcda, xmcda_exec_results);	
		if (xmcda_exec_results.isError())
			return null;
		
		extractAlternatives(inputs, xmcda, xmcda_exec_results);
		checkPreferences(inputs, xmcda, xmcda_exec_results);
		if (xmcda_exec_results.isError())
			return null;
		
		extractPreferences(inputs, xmcda, xmcda_exec_results);	
		checkExtractedPreferences(inputs, xmcda_exec_results);
		if (xmcda_exec_results.isError())
			return null;
		
		return inputs;
	}


	private static void checkParameters(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		AlgorithmType algorithmType = null;


		if (xmcda.programParametersList.size() > 1) {
			errors.addError("Only one list of parameters is expected");
			return;
		}
		if (xmcda.programParametersList.size() == 0) {
			errors.addError("List of parameters not found");
			return;
		}
		if (xmcda.programParametersList.get(0).size() != 4) {
			errors.addError("Exactly four parameter are expected");
			return;
		}

		//final ProgramParameter<?> prgParam = xmcda.programParametersList.get(0).get(0);

		for(ProgramParameter<?> prgParam : xmcda.programParametersList.get(0))
		{
			if ("algorithm_type".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("algorithm_type parameter must have a single (label) value only");
					return;
				}
				try {
					final String parameterValue = (String) prgParam.getValues().get(0).getValue();
					algorithmType = AlgorithmType.fromString((String) parameterValue);
				} catch (Throwable throwable) {
					StringBuffer valid_values = new StringBuffer();
					for (AlgorithmType op : AlgorithmType.values()) {
						valid_values.append(op.getLabel()).append(", ");
					}
					String err = "Invalid value for algorithm_type parameter, it must be a label, ";
					err += "possible values are: " + valid_values.substring(0, valid_values.length() - 2);
					errors.addError(err);
					algorithmType = null;
				}
				inputs.algorithmType = algorithmType;
				
			}
			else if ("check_convergence".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("check_convergence parameter must have a single (boolean) value only");
					return;
				}
				try {
					inputs.checkConvergence = (Boolean) prgParam.getValues().get(0).getValue();

				} catch (Throwable throwable) {					
					String err = "Invalid value for check_convergence parameter, it must be a boolean type, ";
					err += "possible values are: true, false";
					errors.addError(err);
				}
				
			}
			else if ("number_of_iteration".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("check_convergence parameter must have a single (integer) value only");
					return;
				}
				try {
					inputs.numberOfIteration = (Integer) prgParam.getValues().get(0).getValue();
					if(inputs.numberOfIteration<1) {
						errors.addError("number_of_iteration must be positive greater than 1.");
					}
				} catch (Throwable throwable) {
					
					String err = "Invalid value for algorithm_type parameter, it must be a integer.";

					errors.addError(err);
				}
			}
			else if ("early_stopping".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("early_stopping parameter must have a single (boolean) value only");
					return;
				}
				try {
					inputs.earlyStopping = (Boolean) prgParam.getValues().get(0).getValue();

				} catch (Throwable throwable) {					
					String err = "Invalid value for early_stopping parameter, it must be a boolean type, ";
					err += "possible values are: true, false";
					errors.addError(err);
				}
				
			}
			else{

				errors.addError(String.format("Invalid parameter '%s'", prgParam.id()));
				return;
				
			}
		}
		
		

		
	}

	private static void checkAlternatives(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		if (xmcda.alternatives.size() == 0) {
			errors.addError("Alternatives not found");
			return;
		}
		if (xmcda.alternatives.getActiveAlternatives().size() == 0) {
			errors.addError("Active alternatives not found");
			return;
		}
	}

	private static void checkPreferences(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		if (xmcda.alternativesMatricesList.isEmpty()) {
			errors.addError("List of preferences is empty");
			return;
		}
		if (xmcda.alternativesMatricesList.size() == 0) {
			errors.addError("List of preferences has not been supplied");
			return;
		}
		if (xmcda.alternativesMatricesList.size() != 1) {
			errors.addError("Exactly one list of preferences is expected");
			return;
		}
		if (xmcda.alternativesMatricesList.get(0).isEmpty()) {
			errors.addError("List of preferences is empty");
			return;
		}
		if (! xmcda.alternativesMatricesList.get(0).isSimple()) {
			errors.addError("Preferences can only contain single values for a pair of alternatives.");
			return;
		}
		if (! xmcda.alternativesMatricesList.get(0).isHomogeneous()) {
			errors.addError("Preferences can only contain values of the same type.");
			return;
		}
		try {
			xmcda.alternativesMatricesList.get(0).checkConversion(Double.class);
		} catch (ConversionException e) {
			errors.addError("List of preferences contains illegal preference values, only real values are allowed.");
		}
	}

	/**
	 *
	 * @param inputs
	 * @param xmcda
	 * @param xmcda_execution_results
	 * @return
	 */
	protected static Inputs extractInputs(Inputs inputs, XMCDA xmcda, ProgramExecutionResult xmcda_execution_results) {
		extractAlternatives(inputs, xmcda, xmcda_execution_results);
		extractPreferences(inputs, xmcda, xmcda_execution_results);
		checkExtractedPreferences(inputs, xmcda_execution_results);
		return inputs;
	}


	private static void extractAlternatives(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		List<String> alternativesIds = xmcda.alternatives.getActiveAlternatives().stream()
				.filter(a -> "alternatives.xml".equals(a.getMarker())).map(Alternative::id)
				.collect(Collectors.toList());
		if (alternativesIds.isEmpty())
			errors.addError("The alternatives list can not be empty.");
		inputs.alternatives_ids = alternativesIds;
	}

	private static void extractPreferences(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		@SuppressWarnings("unchecked")
		AlternativesMatrix<Double> preferences = (AlternativesMatrix<Double>) xmcda.alternativesMatricesList.get(0);
		inputs.preferences = new LinkedHashMap<String, Map<String, Double>>();

		for (Coord<Alternative, Alternative> coord : preferences.keySet()) {
			String x = coord.x.id();
			String y = coord.y.id();
			Double value = preferences.get(coord).get(0).getValue().doubleValue();
			inputs.preferences.putIfAbsent(x, new HashMap<>());
			inputs.preferences.get(x).put(y, value);
		}
	}

	private static void checkExtractedPreferences(Inputs inputs, ProgramExecutionResult errors) {
		if ((inputs.alternatives_ids != null) && (inputs.preferences != null)) {
			for (String alternative : inputs.alternatives_ids) {
				if (inputs.preferences.containsKey(alternative)) {
					for (String alternative2 : inputs.alternatives_ids) {
						if (! inputs.preferences.get(alternative).containsKey(alternative2)) {
							errors.addError(
									"In list of preferences doesn't exist preference to alternatives: " + alternative + " and "+alternative2);
							return;
						}
						if ( inputs.preferences.get(alternative).get(alternative2) <0) {
							errors.addError(
									"The preference value of alternatives "+alternative+" and "+alternative2+" must be positive. ");
							return;
						}
					}
				}else {
					errors.addError("In list of preferences doesn't exist alternative: " + alternative);
					return;
				}
			}
		}
		
		
		

	}
}
