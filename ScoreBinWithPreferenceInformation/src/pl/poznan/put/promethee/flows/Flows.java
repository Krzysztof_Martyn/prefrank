package pl.poznan.put.promethee.flows;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import pl.poznan.put.promethee.algorithms.BasicAlgorithm;
import pl.poznan.put.promethee.algorithms.Hits;
import pl.poznan.put.promethee.algorithms.Matrix;
import pl.poznan.put.promethee.algorithms.PageRank;
import pl.poznan.put.promethee.algorithms.Salsa;
import pl.poznan.put.promethee.algorithms.TrustHits;
import pl.poznan.put.promethee.algorithms.TrustRank;
import pl.poznan.put.promethee.algorithms.TrustSalsa;
import pl.poznan.put.promethee.xmcda.FlowsResult;
import pl.poznan.put.promethee.xmcda.InputsHandler.AlgorithmType;
import pl.poznan.put.promethee.xmcda.InputsHandler.Inputs;

public class Flows {

	public static FlowsResult calcFlows(Inputs inputs) {
		Map<String, Map<String, Double>> flows = new LinkedHashMap<String, Map<String, Double>>();

		BasicAlgorithm algorithmStrenght = null;
		BasicAlgorithm algorithmWeakness = null;

		List<String> index =  inputs.alternatives_ids;
		
		
		Matrix tr_positive = new Matrix(inputs.alternatives_strenght,index, 1/index.size());
		Matrix tr_negative = new Matrix(inputs.alternatives_weakness,index, 1/index.size());
		double q =inputs.q;
		
		if(inputs.algorithmType == AlgorithmType.TYPE_1) 
		{
			if(inputs.alternatives_strenght.isEmpty()) 	algorithmStrenght = new PageRank(inputs.preferences, index);
			else										algorithmStrenght = new TrustRank(inputs.preferences, index,tr_positive,q);
			
			if(inputs.alternatives_weakness.isEmpty())	algorithmWeakness = new PageRank(inputs.preferences, index,true);
			else										algorithmWeakness = new TrustRank(inputs.preferences, index,tr_negative,q,true);
			
		}
		else if(inputs.algorithmType ==AlgorithmType.TYPE_2) 
		{
			
			if(inputs.alternatives_strenght.isEmpty())	algorithmStrenght = new Hits(inputs.preferences, index);
			else										algorithmStrenght = new TrustHits(inputs.preferences, index,tr_positive,q);
			
			if(inputs.alternatives_weakness.isEmpty())	algorithmWeakness = new Hits(inputs.preferences, index,true);
			else										algorithmWeakness = new TrustHits(inputs.preferences, index,tr_negative,q,true);
		}
		else 
		{

			if(inputs.alternatives_strenght.isEmpty())	algorithmStrenght = new Salsa(inputs.preferences, index);
			else										algorithmStrenght = new TrustSalsa(inputs.preferences, index,tr_positive,q);
			
			if(inputs.alternatives_weakness.isEmpty())	algorithmWeakness = new Salsa(inputs.preferences, index,true);
			else										algorithmWeakness = new TrustSalsa(inputs.preferences, index,tr_negative,q,true);
		}
		
		
		algorithmStrenght.run(inputs.numberOfIteration, inputs.checkConvergence, inputs.earlyStopping);
		algorithmWeakness.run(inputs.numberOfIteration, inputs.checkConvergence, inputs.earlyStopping);
		Matrix total = algorithmStrenght.Get().subtract(algorithmWeakness.Get());
		
		Map<String, Double> positiveFlows = algorithmStrenght.GetResult();
		Map<String, Double> negativeFlows = algorithmWeakness.GetResult();
		Map<String, Double> totalFlows = total.GetResult();
		
		
		flows.put("positive_flows", sortMapByKey(positiveFlows));
		flows.put("negative_flows", sortMapByKey(negativeFlows));
		flows.put("total_flows", sortMapByKey(totalFlows));
		

		return new FlowsResult(flows, 
				algorithmStrenght.isConvergent(),
				algorithmWeakness.isConvergent());
	}


	private static Map<String, Double> sortMapByKey(Map<String, Double> map) {
		Map<String, Double> sortedMap = map.entrySet().stream().sorted(Entry.comparingByKey())
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		return sortedMap;
	}
}
