package pl.poznan.put.promethee.xmcda;

import org.xmcda.Alternative;
import org.xmcda.AlternativesMatrix;
import org.xmcda.AlternativesValues;
import org.xmcda.ProgramExecutionResult;
import org.xmcda.QualifiedValue;
import org.xmcda.QualifiedValues;
import org.xmcda.XMCDA;
import org.xmcda.utils.Coord;

import javafx.util.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public class OutputsHandler {
	/**
	 * Returns the xmcda v3 tag for a given output
	 * 
	 * @param outputName
	 *            the output's name
	 * @return the associated XMCDA v2 tag
	 * @throws NullPointerException
	 *             if outputName is null
	 * @throws IllegalArgumentException
	 *             if outputName is not known
	 */
	public static final String xmcdaV3Tag(String outputName) {
		switch (outputName) {
		case "positive_flows":
			return "alternativesValues";
		case "negative_flows":
			return "alternativesValues";
		case "total_flows":
			return "alternativesValues";
        case "ranking":
            return "alternativesMatrix";
		case "messages":
			return "programExecutionResult";
		default:
			throw new IllegalArgumentException(String.format("Unknown output name '%s'", outputName));
		}
	}

	/**
	 * Returns the xmcda v2 tag for a given output
	 * 
	 * @param outputName
	 *            the output's name
	 * @return the associated XMCDA v2 tag
	 * @throws NullPointerException
	 *             if outputName is null
	 * @throws IllegalArgumentException
	 *             if outputName is not known
	 */
	public static final String xmcdaV2Tag(String outputName) {
		switch (outputName) {
		case "positive_flows":
			return "alternativesValues";
		case "negative_flows":
			return "alternativesValues";
		case "total_flows":
			return "alternativesValues";
        case "ranking":
            return "alternativesComparisons";
		case "messages":
			return "methodMessages";
		default:
			throw new IllegalArgumentException(String.format("Unknown output name '%s'", outputName));
		}
	}

	/**
	 * Converts the results of the computation step into XMCDA objects.
	 * 
	 * @param weights
	 * @param executionResult
	 * @return a map with keys being xmcda objects' names and values their
	 *         corresponding XMCDA object
	 */
	private static void convertFlow(Map<String, XMCDA> x_results, Map<String, Map<String, Double>> flows,
			ProgramExecutionResult executionResult) {
		for (String flowName : flows.keySet()) {
			XMCDA xmcda = new XMCDA();
			Map<String, Double> flow = flows.get(flowName);
			AlternativesValues<Double> result = new AlternativesValues<Double>();
			for (String alternativeID : flow.keySet()) {
				Double value = flow.get(alternativeID).doubleValue();
				Alternative alternative = new Alternative(alternativeID);
				result.put(alternative, value);
			}
			xmcda.alternativesValuesList.add(result);
			x_results.put(flowName, xmcda);
		}
	}
	private static void convertRanking(Map<String, XMCDA> x_results,List<String> alternatives, Map<Pair<String, String>, Integer> alternativesComparison, ProgramExecutionResult executionResult)
    {
        XMCDA xmcda = new XMCDA();
        AlternativesMatrix<Double> result = new AlternativesMatrix<Double>();

        for(String alternative1: alternatives) {
            for (String alternative2 : alternatives) {
                if (alternativesComparison.keySet().contains(new Pair<String, String>(alternative1, alternative2))) {
                    Double value = alternativesComparison.get(new Pair<String, String>(alternative1, alternative2)).doubleValue();
                    Alternative alt1 = new Alternative(alternative1);
                    Alternative alt2 = new Alternative(alternative2);
                    Coord<Alternative, Alternative> coord = new Coord<Alternative, Alternative>(alt1, alt2);
                    QualifiedValues<Double> values = new QualifiedValues<Double>(new QualifiedValue<Double>(value));
                    result.put(coord, values);
                }
            }
        }

        result.setMcdaConcept("atLeastAsGoodAs");
        xmcda.alternativesMatricesList.add(result);
        x_results.put("ranking", xmcda);

    }
	public static Map<String, XMCDA> convert(Map<String, Map<String, Double>> flows,
			Map<Pair<String, String>,Integer> ranking,
			List<String> alternatives,
			ProgramExecutionResult executionResult){
		Map<String, XMCDA> x_results = new HashMap<>();
		
		convertFlow(x_results,flows, executionResult);
		convertRanking(x_results,alternatives,ranking, executionResult);

		return x_results;
	}
}
