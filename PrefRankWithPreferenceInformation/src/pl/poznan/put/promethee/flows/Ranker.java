package pl.poznan.put.promethee.flows;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.util.Pair;

public class Ranker {
	public static Map<Pair<String, String>,Integer>Ranking(List<String> alternatives_ids , Map<String, Map<String, Double>> flows){
		Map<Pair<String, String>,Integer> result = new HashMap<>();
		Map<String, Double> positiveFlow = flows.get("positive_flows");
		Map<String, Double> negativeFlow = flows.get("negative_flows");
	    for (String alternativeA : alternatives_ids) {
	        for (String alternativeB : alternatives_ids) {
	            if(!alternativeA.equalsIgnoreCase(alternativeB)) {
	                if((positiveFlow.get(alternativeA) > positiveFlow.get(alternativeB) &&
	                    negativeFlow.get(alternativeA) < negativeFlow.get(alternativeB)) ||
	                    (positiveFlow.get(alternativeA) > positiveFlow.get(alternativeB) &&
	                    negativeFlow.get(alternativeA).equals(negativeFlow.get(alternativeB))) ||
	                    (positiveFlow.get(alternativeA).equals(positiveFlow.get(alternativeB)) &&
	                    negativeFlow.get(alternativeA) < negativeFlow.get(alternativeB)) ||
	                    (positiveFlow.get(alternativeA).equals(positiveFlow.get(alternativeB))) &&
	                    (negativeFlow.get(alternativeA).equals(negativeFlow.get(alternativeB)))) {
	                        result.put(new Pair<>(alternativeA, alternativeB), 1);
	                }
	            }
	        }
	    }
	    return result;
	}
}
