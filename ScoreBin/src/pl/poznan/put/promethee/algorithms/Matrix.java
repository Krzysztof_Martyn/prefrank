package pl.poznan.put.promethee.algorithms;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class Matrix {

	protected int nrows;
	protected int ncols;
	protected double[][] data;
	protected String[] index;
    
	
	
	public Matrix(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids)
	{
		this.nrows = matrix.size();
		this.ncols = matrix.size();
		this.data = new double[this.nrows][this.ncols];
		
		this.index =  alternatives_ids.toArray(new String[0]);
		for(int i =0;i<this.nrows;i++)
		{
			for(int j =0;j<this.ncols;j++)
			{
				this.data[i][j] = matrix.get(this.index[i]).get(this.index[j]);
			}
		}		
	}
	
	public Matrix(Map<String, Double> matrix,List<String> index, double init_value)
	{
		this.nrows = index.size();
        this.ncols = 1;
        this.data = new double[this.nrows][this.ncols];
        this.index = index.toArray(new String [0]);
        if(matrix.isEmpty()) {
        	this.init(init_value);
        }
        else {
	        this.clear();
	       
	        for (Entry<String, Double> entry : matrix.entrySet()) {
	        	int i = index.indexOf(entry.getKey());
	        	this.data[i][0] = entry.getValue();
	        }
        }
        	
	}
	
	public Matrix (Matrix other)
	{
		this(other.nrows,other.ncols,other.index);
		
	}
	
	public Matrix(int nrow, int ncol,String[] index) {
        this.nrows = nrow;
        this.ncols = ncol;
        this.data = new double[this.nrows][this.ncols];
        this.index = index;
    }
	
	public Matrix(int nrow, String[] index) {
        this.nrows = nrow;
        this.ncols = 1;
        this.data = new double[nrow][1];
        this.index = index;
        
    }
	
	public void clear() {

        for (int i = 0; i < this.nrows; i++) {
            for (int j = 0; j < this.ncols; j++) {
                this.data[i][j]=0.;
            }
        }
	}
	public Matrix Copy() {
		Matrix cp = new Matrix(this);
		
		for(int i =0;i<this.nrows;i++)
		{
			for(int j =0;j<this.ncols;j++)
			{
				cp.data[i][j] = this.data[i][j];
			}
		}	
		return cp;
	}
	
	public void init(double val) {

        for (int i = 0; i < this.nrows; i++) {
            for (int j = 0; j < this.ncols; j++) {
                this.data[i][j]=val;
            }
        }
	}
	public void _normailzeColumns()
	{
		for (int j = 0; j < this.ncols; j++) {
			double sum =0.;
			for(int i =0;i<this.nrows;i++)
			{
				sum+=this.data[i][j];
			}
			if(sum>0) 
			{
				for(int i =0;i<this.nrows;i++)
				{
					this.data[i][j] /= sum;
				}
			}
		}
	}
	public void _normailzeRows()
	{
		for(int i =0;i<this.nrows;i++)
		{
			double sum =0.;
			for (int j = 0; j < this.ncols; j++)
			{
				sum+=this.data[i][j];
			}
			if(sum>0) 
			{
				for (int j = 0; j < this.ncols; j++)
				{
					this.data[i][j] /= sum;
				}
			}
		}
	}
	public Matrix normailzeColumns()
	{
		Matrix out = new Matrix(this);
		out.init(0);
		for (int j = 0; j < this.ncols; j++) {
			double sum =0.;
			for(int i =0;i<this.nrows;i++)
			{
				sum+=this.data[i][j];
			}
			if(sum>0) 
			{
				for(int i =0;i<this.nrows;i++)
				{
					out.setValueAt(i,j, this.data[i][j]/ sum);
				}
			}
		}
		return out;
	}
	public Matrix normailzeRows()
	{
		Matrix out = new Matrix(this);
		out.init(0);
		for(int i =0;i<this.nrows;i++)
		{
			double sum =0.;
			for (int j = 0; j < this.ncols; j++)
			{
				sum+=this.data[i][j];
			}
			if(sum>0) 
			{
				for (int j = 0; j < this.ncols; j++)
				{
					out.setValueAt(i,j, this.data[i][j]/ sum);
				}
			}
		}
		return out;
	}
	public Matrix transpose() {
	    Matrix transposedMatrix = new Matrix(this);
	    for (int i=0;i<this.nrows;i++) {
	        for (int j=0;j<this.ncols;j++) {
	            transposedMatrix.setValueAt(j, i, this.getValueAt(i, j));
	        }
	    }
	    return transposedMatrix;
	}

	public Matrix add(Matrix other) {
	    Matrix result = new Matrix(this);
	    for (int i=0;i<this.nrows;i++) {
	        for (int j=0;j<this.ncols;j++) {
	        	result.setValueAt(i, j, this.getValueAt(i,j)+other.getValueAt(i, j));
	        }
	    }
	    return result;
	}
	public Matrix subtract(Matrix other) {
	    Matrix result = new Matrix(this);
	    for (int i=0;i<this.nrows;i++) {
	        for (int j=0;j<this.ncols;j++) {
	        	result.setValueAt(i, j, this.getValueAt(i,j)-other.getValueAt(i, j));
	        }
	    }
	    return result;
	}
	public Matrix mul( double a) {
		Matrix out = new Matrix(this);
		for (int i = 0; i < this.nrows; i++) { // aRow
			 for (int j = 0; j < this.ncols; j++) { // aColumn
				 out.setValueAt(i, j, this.getValueAt(i, j)*a);
			 }
		 }
		return out;	
	}
	
	public Matrix _mul( double a) {
		for (int i = 0; i < this.nrows; i++) { // aRow
			 for (int j = 0; j < this.ncols; j++) { // aColumn
				 this.setValueAt(i, j, this.getValueAt(i, j)*a);
			 }
		 }
		return this;	
	}
	

	
    public Matrix mul( Matrix B) {


        int bRows = B.nrows;
        int bColumns = B.ncols;

        if (this.ncols != bRows) {
            throw new IllegalArgumentException("A:Rows: " + this.ncols + " did not match B:Columns " + bRows + ".");
        }

        Matrix C = new Matrix(this.nrows,bColumns,this.index);
        C.clear();

        for (int i = 0; i < this.nrows; i++) { // aRow
            for (int j = 0; j < bColumns; j++) { // bColumn
                for (int k = 0; k < this.ncols; k++) { // aColumn
                	C.addValueAt(i, j, this.getValueAt(i, k) * B.getValueAt(k, j));                    
                }
            }
        }

        return C;
    }
    
    public String getIndex(int i) {
    	return this.index[i];
    }
    
	public void addValueAt(int i, int j, double valueAt) {
		// TODO Auto-generated method stub
		 this.data[i][j] += valueAt;
	}
	
	public void setValueAt(int i, int j, double valueAt) {
		// TODO Auto-generated method stub
		 this.data[i][j] = valueAt;
	}

	public double getValueAt(int i, int j) {
		// TODO Auto-generated method stub
		return this.data[i][j];
	} 
	
	public LinkedHashMap<String, Double> GetResult(){
		return this.GetResult(0);
	}
	public LinkedHashMap<String, Double> GetResult(int column){
		LinkedHashMap<String, Double> result = new LinkedHashMap<String, Double>();
		for(int i = 0; i < this.nrows ;i++)
		{
			result.put(this.getIndex(i),Format(this.getValueAt(i, column)));
		}
		
		return result;
		
	}
	private static double Format(double number) {
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
	
	private String round(double data2)
	{
		if(data2==0)
			return "0";
		else
			return String.format("%.2f", data2);
		
	}

	public void Print() {
       for (int i = 0; i < this.nrows; i++) {
            for (int j = 0; j < this.ncols; j++) {
                System.out.print(round(this.data[i][j]));
                System.out.print(",\t");
            }
            System.out.println("");
        }
        System.out.println("");
	}
	
	public boolean CheckAbsLowerThan(double a) {
		for (int i = 0; i < this.nrows; i++) { // aRow
			 for (int j = 0; j < this.ncols; j++) { // aColumn
				 if (Math.abs(this.getValueAt(i, j)) > a)
					 return false;
			 }
		 }
		return true;	
	}
	public double ElemSum() {
		double sum = 0;
		for (int i = 0; i < this.nrows; i++) { // aRow
			 for (int j = 0; j < this.ncols; j++) { // aColumn
				 sum += this.getValueAt(i, j);
			 }
		 }
		return sum;	
	}
	
}
