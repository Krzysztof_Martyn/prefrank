package pl.poznan.put.promethee.xmcda;

import java.util.Map;

public class FlowsResult {
	private Map<String, Map<String, Double>> flows;
	private boolean isConvergentStrenght;
	private boolean isConvergentWeakness;

	
	public FlowsResult(Map<String, Map<String, Double>> flows, boolean isConvergentStrenght,
			boolean isConvergentWeakness) {
		super();
		this.flows = flows;
		this.isConvergentStrenght = isConvergentStrenght;
		this.isConvergentWeakness = isConvergentWeakness;
	}
	public Map<String, Map<String, Double>> getFlows() {
		return flows;
	}
	public boolean isConvergentStrenght() {
		return isConvergentStrenght;
	}
	public boolean isConvergentWeakness() {
		return isConvergentWeakness;
	}
}
